FROM ubuntu:focal
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install krb5-kdc krb5-admin-server pwgen gettext-base -y

COPY templates /root/templates

ADD entrypoint.sh /
CMD /entrypoint.sh