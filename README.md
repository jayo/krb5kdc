# kdc-tester

A docker compose environment for setting up a Kerberos KDC ([Key Distribution Center](https://web.mit.edu/kerberos/krb5-latest/doc/admin/install_kdc.html)) for rudimentary testing.

It's largely adapted from our [development instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kerberos.md) - but oriented for using it in a deployed testing environment (with working DNS/name resolution), rather than a local development environment.

The purpose of the setup is to:

* Stand up an MIT KDC in a Ubuntu-based container
* Create a rudimentary Kerberos Realm and Kerberos database at container startup
* Create a service principal and add that to a Kerberos [Keytab](https://web.mit.edu/kerberos/krb5-devel/doc/basic/keytab_def.html) file for GitLab to verify a client-provided Kerberos [ticket](https://web.mit.edu/kerberos/krb5-devel/doc/user/tkt_mgmt.html)
* Create a single user principal for testing with a random password dumped to `stdout` for use with [kinit](https://web.mit.edu/kerberos/krb5-1.12/doc/user/user_commands/kinit.html)

## Use

1. Clone this to a host where you can run a docker container and expose port `88` (or change the advertised port to something you can expose, and )
1. Create [a `.env` file](https://docs.docker.com/compose/env-file/) or otherwise expose the following environment vars:
  * KERBEROS_DOMAIN: DNS name to identify the Kerberos realm, should be uppercase by convention (e.g. EXAMPLE.COM )
  * KERBEROS_KDC: DNS name for the KDC / host running the container (e.g. kdc.example.com)
  * GITLAB_HOST: GitLab hostname, used to create the service principal (e.g. gitlab.example.com)
  * GITLAB_USER_PRINCIPAL: User principal/account created for use with kinit
1. Run `docker compose up`
1. The generated password for the user principal will be echo'd to docker container's `stdout`:

  ```
  [...]
  kdc  | Principal "testme@EXAMPLE.COM" created.
  kdc  |
  kdc  | *** Created a principal for testme@EXAMPLE.COM - password: <42 character random string>
  kdc  |
  ```

The templated `krb5.conf` (suitable for use on clients and the) can be copied out of the container by using `docker compose cp krb5:/etc/krb5.conf .`

The generated keytab that you'll need for use with GitLab will can be copied out of the container by using `docker compose cp krb5:/etc/krb5kdc/gitlab.keytab .`

## Notes

### krb5.conf location

`/etc/krb5.conf` is the standard location for the client configuration file (for both macOS and Linux).  You can also set `KRB5_CONFIG` in the environment to point to an alternate location
### macOS and UDP

The docker container exposes the KDC port on both UDP and TCP for testing with `kinit` on MacOS.  MacOS will not fall back to TCP to communicate with the KDC.

You can edit the generated `krb5.conf` to specify `tcp` as the protocol to connect to the specified kdc if UDP is blocked (or you don't want to expose it).

See this [stackexchange post](https://apple.stackexchange.com/questions/63122/kinit-krb5-get-init-creds-unable-to-reach-any-kdc-in-realm-local) for details.

### Debugging kinit

You can set `KRB5_TRACE=/dev/stderr` in the environment to get tracing details for `kinit` and other kerberos CLI utilities.

**NOTE** Do not set `KRB5_TRACE=/dev/stdout` when using Git-over-HTTPS - the git commands will fail with a 128 exit code due to contention on what's been piped to `stdout`








