#!/bin/bash
set -e

# make sure that the following environment variables have been set!
# KERBEROS_DOMAIN
# KERBEROS_KDC
# GITLAB_HOST
# GITLAB_USER_PRINCIPAL

bootstrap_kdc() {
  echo "Bootstrapping kdc..."

  # creat configuration files
  mkdir -p /etc/krb5kdc
  cat /root/templates/kdc.conf.tmpl | envsubst > /etc/krb5kdc/kdc.conf
  cat /root/templates/kadm5.acl | envsubst > /etc/krb5kdc/kadm5.acl
  cat /root/templates/krb5.conf.tmpl | envsubst > /etc/krb5.conf

  kdc_master_password=$(pwgen -cn1 42)
  # initialize the kdc database
  kdb5_util -P ${kdc_master_password} -r ${KERBEROS_DOMAIN} create -s
  kadmin.local -q 'addprinc root/admin'
  kadmin.local -q "addprinc -randkey kadmin/${KERBEROS_KDC}"
  kadmin.local -q "ktadd kadmin/${KERBEROS_KDC}"

  # create a service principal and keytab for the gitlab host
  kadmin.local -q "addprinc -randkey HTTP/${GITLAB_HOST}"
  kadmin.local -q "ktadd -k /etc/gitlab.keytab HTTP/${GITLAB_HOST}"
  echo -e "\n*** Created /etc/gitlab.keytab for HTTP/${GITLAB_HOST}@${KERBEROS_DOMAIN}\n"

  # created a service host principal for the gitlab host
  kadmin.local -q "addprinc -randkey host/${GITLAB_HOST}"
  kadmin.local -q "ktadd -k /etc/gitlab.keytab host/${GITLAB_HOST}"
  echo -e "\n*** Created /etc/gitlab.keytab for host/${GITLAB_HOST}@${KERBEROS_DOMAIN}\n"


  # create a user principal and echo the password
  user_principal_password=$(pwgen -cn1 42)
  kadmin.local -q "addprinc -pw ${user_principal_password} ${GITLAB_USER_PRINCIPAL}"
  echo -e "\n*** Created a principal for ${GITLAB_USER_PRINCIPAL}@${KERBEROS_DOMAIN} - password: ${user_principal_password}\n"

  # bootstrapped!
  echo $(date) > /root/bootstrapped
}


if [[ ! -e /root/bootstrapped ]]; then
        bootstrap_kdc
fi

echo "Starting KDC..."
/usr/sbin/krb5kdc -n && /usr/sbin/kadmind -nofork
